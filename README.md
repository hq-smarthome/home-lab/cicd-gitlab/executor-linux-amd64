# CICD GitLab AMD64 Linux Executor

> :warning: **This project has been moved**
>
> Any further updates can be found here https://gitlab.com/carboncollins-cloud/cicd/gitlab-linux-amd64-executor
> This repository will be archived in favour of all further development at the new location.


[[_TOC_]]

## Description

A parametised batch job intended to be be dispatched by [CICD GitLab Runner](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/runner) for running Linux AMD64 based jobs.

## Docker Image

This job uses the common [CICD GitLab Executor Docker Image](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-docker) as it's base image which includes all of the nessasary build dependencies for jobs within the hQ project.

## Deployments

This job is deployed as a parametised batch job to the cluster and is intended to be dispatched by a [CICD GitLab Runner](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/runner). This allows for a pre-defined executor setup to be registered into Nomad and prevents arbitary jobs from being deployed by the runner (It only has permissions to dispatch these executors).

This jobs deployment is also intended to be triggered by builds on the [CICD GitLab Executor Docker Image](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-docker) and not manually.

## Other Info

This repo has been moved to: https://gitlab.com/carboncollins-cloud/cicd/gitlab-linux-amd64-executor. This existing repo will remain here archived as there are existing links to this repo.
