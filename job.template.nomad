job "cicd-gitlab-linux-amd64-executor" {
  type = "batch"
  region = "global"
  datacenters = ["proxima"]
  namespace = "cicd-gitlab"

  parameterized {
    meta_required = [
      "executor_id"
    ]
  }

  group "executor" {
    count = 1

    meta {
      EXECUTOR_ID = "${NOMAD_META_executor_id}"
    }

    constraint {
      attribute = "${attr.cpu.arch}"
      value = "amd64"
    }

    constraint {
      attribute = "${attr.kernel.name}"
      value = "linux"
    }

    network {
      mode = "bridge"
    }

    task "executor" {
      driver = "docker"

      config {
        image = "[[ .executorImageName ]]"
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        EXECUTOR_NAME = "${NOMAD_ALLOC_ID}"
        EXECUTOR_PLATFORM = "linux-amd64"

        VAULT_ADDR = "https://vault.hq.carboncollins.se"
        CONSUL_ADDR = "https://consul.hq.carboncollins.se"
        NOMAD_ADDR = "https://nomad.hq.carboncollins.se"
        NOMAD_NAMESPACE = "default"
      }

      restart {
        attempts = 0
      }
    }
  }

  reschedule {
    attempts = "0"
    unlimited = false
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
